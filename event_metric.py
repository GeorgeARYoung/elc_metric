# import pickle
import numpy as np
import math
from scipy.io import loadmat
import matplotlib.pyplot as plt

# import seaborn as sns
# from leven import levenshtein

# set the window size for the metric
fs_win = 7  # choose winsize=3.5 for 25ms window
fp_win = 7  # chhose winsize=14 for 100 ms window
# read the lebls from the mat file for different laberllers
label_1 = loadmat('G:\My Drive\Projects\Event Detection\dataset\LabellerIdx_7_PrIdx_1_TrIdx_1.mat')
label_2 = loadmat('G:\My Drive\Projects\Event Detection\dataset\LabellerIdx_8_PrIdx_1_TrIdx_1.mat')
data1 = label_1['TrialData']
data2 = label_2['TrialData']
labels_1 = data1['Labels']
labels_2 = data2['Labels']
temp = labels_1[0, 0]
temp2 = labels_2[0, 0]
temp = temp[6]
temp2 = temp2[7]
labeller1 = temp[0][0]
labeller2 = temp2[0][0]
labeller2 = labeller2[0:len(labeller1)]
original = labeller2[454:654]
target1 = original.copy()
target1[86:113] = 3
target1[113:118] = 1
target1[133:136] = 3
target1[142:145] = 3
target1[176:177] = 1
# target2 = np.load('target2.npy')
# target3 = np.load('target3.npy')
# target4 = np.load('target4.npy')

'''################# read the labels from the pickle file #############'''


# PATH_TO_DATASET = 'G:\My Drive\Projects\Event Detection\dataset'
# DATASET = pickle.load(open(PATH_TO_DATASET + '\Dataset.p', "rb"))
# subject1 = DATASET[0]
##trial1 = subject1[0]
# Labels = []
# for trial in subject1:
#    for labels in trial['Labels']:
#        Labels.append(int(labels))
# Labels = np.asarray(Labels)


# find transition points in the original label sequences
def find_tran(Labels):
    transitions = []
    for i in range(len(Labels) - 1):
        if Labels[i] != Labels[i + 1]:
            transitions.append(i)
    return transitions


# Items in the event list:
# start position, end position, event type, transition type(from previous type to current type)
def to_event(transitions, labels):
    events = []
    for idx, transpoint in enumerate(transitions):
        event = []
        handled = False  # whether or not the event has been processed
        if idx == 0:
            event.append(0)
            event.append(transpoint)
            event.append(int(labels[0]))
            tran_type = ''
            tran_type += str(labels[0])
            tran_type += str(labels[0])
            event.append(tran_type)
        else:
            event.append(transitions[idx - 1] + 1)
            event.append(transpoint)
            event.append(int(labels[transitions[idx]]))
            tran_type = ''
            tran_type += str(labels[transitions[idx - 1]])
            tran_type += str(labels[transitions[idx]])
            event.append(tran_type)
        event.append(handled)
        events.append(event)
    event = []
    event.append(transitions[-1] + 1)
    event.append(len(labels) - 1)
    event.append(int(labels[-1]))
    tran_type = ''
    tran_type += str(labels[transitions[-1]])
    tran_type += str(labels[-1])
    event.append(tran_type)
    event.append(handled)
    events.append(event)
    return events


def to_string(events):
    string = ''
    for e in events:
        string = string + str(e[2])
    return string


def inv(str1):
    char0 = str1[0]
    char1 = str1[1]
    str2 = char1 + char0
    return str2


# function used for find corresponding transition points and create tuples
def matching(events_1, events_2):
    # tuple used to store the match values
    # found(0 or 1), shift (integer), position in seq1, trans_type in seq1, trans_type in seq2 
    measures = []
    event_seq1 = events_1.copy()
    event_seq2 = events_2.copy()
    for event in event_seq1:
        idx = event[0]
        if event[3] == '13' or event[3] == '31':  # Fixation-saccade transitions
            window = np.arange(idx - fs_win // 2, idx + fs_win // 2 + 1)
        else:
            window = np.arange(idx - fp_win // 2, idx + fp_win // 2 + 1)
        found = 0
        shift = '-'
        transition = ''
        for event2 in event_seq2:
            if event2[0] in window:
                found = 1
                shift = event2[0] - idx
                transition = event2[3]
                event_seq2.remove(event2)
                break
        measure = [found, shift, event[0], event[3], transition]
        measures.append(measure)
    return measures


def getKey(item):
    return item[2]


# put measure information from both direction together
def combine_measure(m1, m2):
    m = m1 + m2
    return sorted(m, key=getKey)


# remove the matched transition point from tuple list
def remove_match(measures):
    measure_removed = measures.copy()
    for measure in measures:
        if measure[0] == 1 and measure[3] == measure[4]:
            measure_removed.remove(measure)
    return measure_removed


# return the matched event construct from the measures
def matched(measures):
    return [m for m in measures if m[0] == 1 and m[3] == m[4]]


# calculate L2 distance from a pair of adjacent measures that at least one of them is matched
# distance can be derived directly from shifts
def l2dis(m1, m2):
    return math.sqrt(m1 ** 2 + m2 ** 2)


# calculate the overlap ratio from a pair of matched measures
def olr(l1, l2, u1, u2):
    lowerbound = min(l1, l2)
    upperbound = max(u1, u2)
    overlapsrt = max(l1, l2)
    overlapend = min(u1, u2)
    return (overlapend - overlapsrt + 1) / (upperbound - lowerbound + 1)


# transitions_1 = find_tran(labeller1)
# transitions_2 = find_tran(labeller2)
# events1 = to_event(transitions_1,labeller1)
# events2 = to_event(transitions_2,labeller2)
# measure_a2b = matching(events1,events2)
# measure_b2a = matching(events2,events1)
# me_a2b_unmatched = remove_match(measure_a2b)
# me_b2a_unmatched = remove_match(measure_b2a)
# me_a2b_matched = matched(measure_a2b)
# me_b2a_matched = matched(measure_b2a)
# measure_com = combine_measure(measure_a2b,measure_b2a)


###### calculate the distances from the matched transition points #####
# A matched transition point means the previous event and the future event
# in one sequence have the same labels with the corresponding two events in 
# the other sequence
# score contains index, event type, l2 distance, overlap ratio
def process_matched(measure, events, events_check):
    scores = []
    for idx, m in enumerate(measure):
        if m[0] == 1 and m[3] == m[4]:  # transition positions and types both match
            score = []  # create one instance of score for 2 events at current postion
            event_type = int(m[3][1])
            shift_f = m[1]  # shift at the front
            shift_b = 0  # shift at the back
            upper_tran_base = measure[idx + 1][2]  # upperbound in base sequence
            upper_tran_check = 0  # upperbound in test sequence

            if m[1] != '-' and measure[idx + 1][1] != '-':  # have shift values for both start and end points
                shift_b = measure[idx + 1][1]
                upper_tran_check = measure[idx + 1][1] + measure[idx + 1][2]
            else:
                t = measure[idx + 1][2] - 1  # transition position in the baseline sequence
                for idx_e, event in enumerate(events_check):
                    if t in range(event[0], event[1]):
                        if event_type == event[2]:
                            shift_b = t - event[1]
                            upper_tran_check = event[1]
                        else:
                            shift_b = t - event[0] + 1
                            upper_tran_check = event[0] - 1
                        break
            if idx == 0:  # first pair of events
                l2dis_cur = l2dis(shift_f, shift_b)
                olr_cur = olr(m[2], m[2] + m[1], upper_tran_base, upper_tran_check)
                events[idx][-1] = True
                score.append(idx)
                score.append(event_type)
                score.append(l2dis_cur)
                score.append(olr_cur)
                scores.append(score)
            elif idx == len(measure) - 1:  # last pair of events
                l2dis_cur = abs(m[1])
                olr_cur = (events[-1][1] - max(m[2], m[2] + m[1]) + 1) / (events[-1][1] - min(m[2], m[2] + m[1]) + 1)
                events[idx][-1] = True
                score.append(idx)
                score.append(event_type)
                score.append(l2dis_cur)
                score.append(olr_cur)
                scores.append(score)
            else:
                # process the events after the matching point
                if not events[idx][-1]:  # not processed yet
                    l2dis_cur = l2dis(shift_f, shift_b)
                    olr_cur = olr(m[2], m[2] + m[1], upper_tran_base, upper_tran_check)
                    events[idx][-1] = True
                    score.append(idx)
                    score.append(event_type)
                    score.append(l2dis_cur)
                    score.append(olr_cur)
                    scores.append(score)
                # process the events before the matching point
                if not events[idx - 1][-1]:
                    score = []
                    t_pre = measure[idx - 1][2]  # previous transition position in the baseline sequence
                    for idx_e, event in enumerate(events_check):
                        if t_pre in range(event[0], event[1]):
                            if event_type == event[2]:
                                shift_b = t_pre - event[0]
                                lower_tran_check = event[0]
                            else:
                                shift_b = t_pre - event[0] - 1
                                lower_tran_check = event[1] + 1
                            break
                    l2dis_pre = l2dis(shift_f, shift_b)
                    olr_pre = olr(t_pre, lower_tran_check, m[2], m[2] + m[1])
                    events[idx - 1][-1] = True
                    score.append(idx - 1)
                    score.append(event_type)
                    score.append(l2dis_pre)
                    score.append(olr_pre)
                    scores.append(score)
    return scores


# Globally align the two label sequences.
# Change the label sequences and return the new transition points simultaneously
def globalAlignment(measure, a):
    transitions = []
    for m in measure:
        if m[0] == 1:
            shift = m[1] // 2
            #            remain = m[1]%2
            trans_new = m[2] + shift
            if shift > 0:
                a[m[2]:trans_new] = int(m[3][0])
            elif shift < 0:
                a[trans_new:m[2]] = int(m[3][1])
            transitions.append(trans_new)
    return transitions


'''NEED TO CHANGE THIS TO A MAJORITY VOTE IMPLEMENTATION'''


def fillUndefined(seq1, seq2):
    for idx, sample in enumerate(seq1):
        if sample == 0 and seq2[idx] != 0:
            seq1[idx] = seq2[idx]
    for idx, sample in enumerate(seq2):
        if sample == 0 and seq1[idx] != 0:
            seq2[idx] = seq1[idx]


# Plot original label sequences and processed label sequences.
# Mode 1 works with normal event types, mode 0 display difference           
def plotlabels(target, mode):
    N = len(target)
    color = [''] * N
    Y = np.ones((N), dtype=np.uint8)
    if mode == 1:
        for idx, app in enumerate(target):
            if app == 1:  # Fixation
                color[idx] = 'blue'
            elif app == 2:  # Pursuit
                color[idx] = 'purple'
            elif app == 3:  # Saccade
                color[idx] = 'orange'
            elif app == 4:  # Blink
                color[idx] = 'yellow'
            elif app == 10:
                color[idx] = 'black'
            else:
                color[idx] = 'gray'
    elif mode == 0:
        for idx, app in enumerate(target):
            if app == 0:
                color[idx] = 'green'
            else:
                color[idx] = 'red'
    plt.bar(np.arange(N), Y, color=color, width=1, align='center', alpha=0.5)


# Change the labels so that direct differencing can be done to report transition types    
def changeLabel(seq):
    for idx, label in enumerate(seq):
        if label == 4:
            seq[idx] = 10
        elif label == 3:
            seq[idx] = 100
        elif label == 2:
            seq[idx] = 1000
    return seq


def reportEvents(events):
    numF = 0
    numS = 0
    numP = 0
    numB = 0
    numU = 0
    for e in events:
        if e[2] == 0:
            numU += 1
        elif e[2] == 1:
            numF += 1
        elif e[2] == 2:
            numP += 1
        elif e[2] == 3:
            numS += 1
        else:
            numB += 1
    print('Total number of events: %d' % (numB + numF + numP + numS + numU))
    print('Number of detected Fixations: %d' % numF)
    print('Number of detected Pursuits: %d' % numP)
    print('Number of detected Saccades: %d' % numS)
    print('Number of detected Blinks: %d' % numB)
    print('Number of Undefined events: %d' % numU)


episode1 = labeller1
episode2 = labeller2
episode1 = np.asarray(episode1, dtype=np.int)
episode2 = np.asarray(episode2, dtype=np.int)

diff = episode1 - episode2

transitions_3 = find_tran(episode1)
transitions_4 = find_tran(episode2)
events3 = to_event(transitions_3, episode1)
events4 = to_event(transitions_4, episode2)
measure_c2d = matching(events3, events4)
measure_d2c = matching(events4, events3)

''' Calculate the score of perfectly matched events
    the scores contain idx, L2 distance and Overlap Ratio'''

# Report events numbers in each category
reportEvents(events3)
reportEvents(events4)

# Calculate scores for matched events
scores = process_matched(measure_c2d, events3, events4)

print('scores: \n (index/ event type/ L2 distance/ overlap ratio)\n')
print(*scores, sep='\n')
# Use global alignment to shift the events in sequences from different labelers to a common positions
a_align = episode1.copy()
b_align = episode2.copy()
t_a2b = globalAlignment(measure_c2d, a_align)  # unified transition points
t_b2a = globalAlignment(measure_d2c, b_align)
# Here, the a_align and b_align had been changed

# Fill undefined events with majority votes
a_fu = a_align.copy()
b_fu = b_align.copy()
fillUndefined(a_fu, b_fu)

a_changed = a_fu.copy()
b_changed = b_fu.copy()
a_changed = changeLabel(a_changed)
b_changed = changeLabel(b_changed)
# values indicate the labeling difference type
diff_a2b = a_changed - b_changed


# calculate the number of each misclassification type and stored as a dictionary
def misclass_dict(a, b):
    # values indicate the labeling difference type
    diff_a2b = a - b
    # transition points
    trans_diff = find_tran(diff_a2b)
    # clump into "events"
    events_diff = to_event(trans_diff, diff_a2b)
    num_misclass = 0
    diff_type_dict = dict.fromkeys(["FB", "FS", "FP", "BS", "BP", "BF", "SP", "SB", "SF", "PS", "PB", "PF"], 0)
    for event in events_diff:
        diff_type = event[2]
        if diff_type:
            num_misclass += 1
        if diff_type == -9:
            diff_type_dict['FB'] += 1
        elif diff_type == -99:
            diff_type_dict['FS'] += 1
        elif diff_type == -999:
            diff_type_dict['FP'] += 1
        elif diff_type == -90:
            diff_type_dict['BS'] += 1
        elif diff_type == -990:
            diff_type_dict['BP'] += 1
        elif diff_type == -900:
            diff_type_dict['SP'] += 1
        elif diff_type == 9:
            diff_type_dict['BF'] += 1
        elif diff_type == 90:
            diff_type_dict['SB'] += 1
        elif diff_type == 99:
            diff_type_dict['SF'] += 1
        elif diff_type == 900:
            diff_type_dict['PS'] += 1
        elif diff_type == 990:
            diff_type_dict['PB'] += 1
        elif diff_type == 999:
            diff_type_dict['PF'] += 1
    return num_misclass, diff_type_dict


num_misclass, diff_type_dict = misclass_dict(a_changed, b_changed)

# print out the misclassification results
print('Number of mis-classified evnets:', num_misclass)
for key, value in diff_type_dict.items():
    if value:
        print('{0} {1} mis-classified as {2}'.format(value, key[0], key[1]))

### process the editted label sequences ###
# trans_a_after = find_tran(a_changed)
# trans_b_after = find_tran(b_changed)
# events_a_after = to_event(trans_a_after, a_changed)
# events_b_after = to_event(trans_b_after,b_changed)
# measure_final_a2b = matching(events_a_after,events_b_after)
# measure_final_b2a = matching(events_b_after,events_a_after)
# me_a2b_unmatched = remove_match(measure_final_a2b)
# me_b2a_unmatched = remove_match(measure_final_b2a)
# me_a2b_matched = matched(measure_final_a2b)
# me_b2a_matched = matched(measure_final_b2a)
# scores = process_matched(measure_final_a2b,events_a_after)
# print('scores: \n (index/L2 distance/overlap ratio)\n',scores)


'''#### Plot of the results ####'''
print('2 human labelers\' data')
plt.figure(1)
plt.subplot(611)
plt.xticks([])
plt.yticks([])
plt.ylabel('a', rotation=0)
plotlabels(episode1, 1)
plt.subplot(612)
plt.xticks([])
plt.yticks([])
plt.ylabel('b', rotation=0)
plotlabels(episode2, 1)
plt.subplot(613)
plt.xticks([])
plt.yticks([])
plt.ylabel('c', rotation=0)
plotlabels(diff, 0)

# plt.subplot(714)
# plotlabels(a_align,1)
# plt.xticks([])
# plt.yticks([])
# plt.ylabel('a\'',rotation=0)
#
# plt.subplot(715)
# plotlabels(b_align,1)
# plt.xticks([])
# plt.yticks([])
# plt.ylabel('b\'',rotation=0)

plt.subplot(614)
plotlabels(a_fu, 1)
plt.xticks([])
plt.yticks([])
plt.ylabel('a*', rotation=0)

plt.subplot(615)
plotlabels(b_fu, 1)
plt.xticks([])
plt.yticks([])
plt.ylabel('b*', rotation=0)

plt.subplot(616)
plotlabels(diff_a2b, 0)
plt.yticks([])
plt.ylabel('c*', rotation=0)

'''
print('real data vs hand-crafted copy')
plt.figure(2)
plt.subplot(211)
plotlabels(original,1)
plt.subplot(212)
plotlabels(target1,1)
'''
